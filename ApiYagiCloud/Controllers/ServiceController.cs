﻿using ApiYagiCloud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiYagiCloud.Controllers
{
    //[Authorize]
    [RoutePrefix("api/Service")]
    public class ServiceController : ApiController
    {
        [Authorize]
        [Route("GenerarPedidoSap")]
        public IHttpActionResult GenerarPedidoSap(modelSAP model)
        {
            try
            {
                return Ok(Helpers.GenerarPedidoSAP(model));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
    }
}
