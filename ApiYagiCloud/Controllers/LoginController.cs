﻿using ApiYagiCloud.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace ApiYagiCloud.Controllers
{
  
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [AllowAnonymous]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(Login model)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(model.Username) || string.IsNullOrEmpty(model.Password))
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                bool isCredentialValid = true;//Dao.ValidateAccessTester(model);
                if (isCredentialValid)
                {
                    var token = TokenGenerator.GenerateTokenJwt(model.Username);
                    return Ok(token);
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Ok(Helpers.GetException(ex));
            }
        }
        
    }
}
