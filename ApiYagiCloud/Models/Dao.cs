﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ApiYagiCloud.Models
{
    public class Dao
    {
        public static bool ValidateAccessTester(Login login)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("COMERCIAL"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Yagi_Validar_Api", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pv_username", login.Username));
                    cmd.Parameters.Add(new SqlParameter("@pv_password", login.Password));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
                return l_value == 1;
            }
            catch
            {
                throw;
            }
        }

    }
}