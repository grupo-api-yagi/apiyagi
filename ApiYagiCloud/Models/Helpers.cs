﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace ApiYagiCloud.Models
{
    public class Helpers
    {
        public static string ExecuteAPIObject(object json_data, string url, string token)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                if (token != "")
                {
                    client.DefaultRequestHeaders.Add("authorization", token);
                }
                client.BaseAddress = new Uri(url);
                //HTTP POST
                var postTask = client.PostAsJsonAsync("", json_data);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var ff = result.Content.ReadAsStringAsync().Result;
                    return ff;
                }
                else
                {
                    return "Error";
                }
            }
        }

        public static SqlConnection ConnectToSql(string ps_name)
        {
            string ls_connection = System.Configuration.ConfigurationManager.ConnectionStrings[ps_name].ConnectionString;
            return new SqlConnection(ls_connection);
        }
        public static string GetAppSetting(string ps_name)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings.Get(ps_name);
            return value;
        }
        public static Response GetMessageCrud(DataTable p_dt)
        {
            string l_message = null;
            string l_code = null;
            try
            {
                foreach (DataRow row in p_dt.Rows)
                {
                    l_message = row["MESSAGE"].ToString();
                    l_code = row["VALUE"].ToString();
                }
                return new Response() { errorCode = l_code, message = l_message };
            }
            catch
            {
                throw;
            }
        }
    
        public static Response GetException(Exception p_e)
        {
            string l_message = p_e.Message;
            string l_exception = l_message +
                "\nAn exception occurred.\n" +
                "- Type : " + p_e.GetType().Name + "\n" +
                "- Message: " + p_e.Message + "\n" +
                "- Stack Trace: " + p_e.StackTrace;
            Exception l_ie = p_e.InnerException;
            if (l_ie != null)
            {
                l_exception += "\n";
                l_exception += "The Inner Exception:\n" +
                    "- Exception Name: " + l_ie.GetType().Name + "\n" +
                    "- Message: " + l_ie.Message + "\n" +
                    "- Stack Trace: " + l_ie.StackTrace;
            }
            return new Response() { errorCode = "-1", message = l_exception };
        }
        
        public static void ClearDataTable(DataTable p_dt)
        {
            if (p_dt != null)
            {
                p_dt.Clear();
                p_dt.Dispose();
                p_dt = null;
            }
        }

        public static string GetErrors(List<string> errors)
        {
            string l_message = "";
            foreach (var error in errors) {
                l_message = error + "" + Environment.NewLine;
            }
            return l_message;
        }
        
        public static Response GenerarPedidoSAP(modelSAP model)
        {
            try
            {
                var l_login = new { Username = "user_yagi", Password = "pass_yagi" };
                var l_token = ExecuteAPIObject(l_login, GetAppSetting("Url_Api_Auth"), "");
                l_token = l_token.Replace("\"", "");
                var l_return = ExecuteAPIObject(model, GetAppSetting("Url_Api_GenerarPedido"), l_token);
                var _resp = JsonConvert.DeserializeObject<Response>(l_return);
                return _resp;
            }
            catch
            {
                throw;
            }
        }

    }
}