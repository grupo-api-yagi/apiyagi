﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ApiYagiCloud.Models
{
    public class Response
    {
        public string errorCode { get; set; }
        public string message { get; set; }
        public string fechaHora { get; set; }

    }
    [XmlRoot(ElementName = "Login")]
    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }


    [XmlRoot(ElementName = "Glosa")]
    public class Glosa
    {

        [XmlElement(ElementName = "TipoDespacho")]
        public string TipoDespacho { get; set; }

        [XmlElement(ElementName = "FE")]
        public string FE { get; set; }

        [XmlElement(ElementName = "HE")]
        public string HE { get; set; }

        [XmlElement(ElementName = "IC")]
        public string IC { get; set; }

        [XmlElement(ElementName = "CE")]
        public string CE { get; set; }
    }

    [XmlRoot(ElementName = "Materiales")]
    public class Materiales
    {

        [XmlElement(ElementName = "Codigo")]
        public string Codigo { get; set; }

        [XmlElement(ElementName = "UnidadMedida")]
        public string UnidadMedida { get; set; }

        [XmlElement(ElementName = "Cantidad")]
        public double Cantidad { get; set; }

        [XmlElement(ElementName = "Centro")]
        public string Centro { get; set; }

        [XmlElement(ElementName = "PrecioUnitario")]
        public double PrecioUnitario { get; set; }
    }

    [XmlRoot(ElementName = "ModelSAP")]
    public class modelSAP
    {

        [XmlElement(ElementName = "RUC")]
        public string RUC { get; set; }
        [XmlElement(ElementName = "CodigoSoldto")]
        public string CodigoSoldto { get; set; }

        [XmlElement(ElementName = "CodigoShipto")]
        public string CodigoShipto { get; set; }

        [XmlElement(ElementName = "OCCliente")]
        public string OCCliente { get; set; }

        [XmlElement(ElementName = "Incoterm")]
        public string Incoterm { get; set; }

        [XmlElement(ElementName = "Check19k")]
        public string Check19k { get; set; }

        [XmlElement(ElementName = "Glosa")]
        public Glosa Glosa { get; set; }

        [XmlElement(ElementName = "Materiales")]
        public List<Materiales> Materiales { get; set; }
    }

    /* 
     public class ModelSAP {
         [Required(AllowEmptyStrings = false, ErrorMessage = "<RUC> es obligatorio")]
         public string RUC { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<CodigoShipto> es obligatorio")]
         public string CodigoShipto { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<OCCliente> es obligatorio")]
         public string OCCliente { get; set; }

         public string Incoterm { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<Check19k> es obligatorio")]
         public string Check19k { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<Glosa> es obligatorio")]
         public Glosa Glosa { get; set; }

         [Required(ErrorMessage = "<Material> es obligatorio")]
         public List<Material> Material { get; set; }

         public ModelSAP() {
             Material = new List<Material>();
         }
     }

     public class Glosa {
         [Required(AllowEmptyStrings = false, ErrorMessage = "<TipoDespacho> es obligatorio")]
         public string TipoDespacho { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<FE> es obligatorio")]
         public string FE { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<HE> es obligatorio")]
         public string HE { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<IC> es obligatorio")]
         public string IC { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<CE> es obligatorio")]
         public string CE { get; set; }
     }

     public class Material {
         [Required(AllowEmptyStrings = false, ErrorMessage = "<Codigo> es obligatorio")]
         public string Codigo { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<UnidadMedida> es obligatorio")]
         public string UnidadMedida { get; set; }

         [Required(ErrorMessage = "<Cantidad> es obligatorio")]
         public int Cantidad { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "<Centro> es obligatorio")]
         public string Centro { get; set; }

         public Decimal PrecioUnitario { get; set; }
     }*/
}