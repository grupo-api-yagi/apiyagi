﻿using ApiYagiCloud.Controllers;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace ApiYagiCloud
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración de rutas y servicios de API
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new TokenValidationHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.XmlFormatter.UseXmlSerializer = true;
            //Retorna en Formato XML
            config.Formatters.Add(new XmlMediaTypeFormatter());
        }
    }
}
